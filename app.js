var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongo = require('mongodb');
var mongoose = require('mongoose');

//Set up default mongoose connection
var mongoDB = 'mongodb://yohann:battlefield2@ds012178.mlab.com:12178/facecast';
mongoose.connect(mongoDB, {
  useMongoClient: true
});
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// Configuration de mongoose
//var db = mongodb('localhost:27017/demorest');

// 

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var restapi = require('./rest/users.js');

var index = require('./routes/index');
var users = require('./routes/users');
var offres = require('./routes/offres');

var updOffre = require('./routes/updOffre');
var figurants = require('./routes/figurants');
var candidatures = require('./routes/candidatures');


/* Configuration de mongoose
mongoose.connect(mongoDB, {
    useMongoClient: true
})

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

*/
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.locals.pretty = true;

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(require('express-session')({ 
    secret: 'keyboard cat',
    resave: false, saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req,res,next){
  req.db = db;
  next();
});

app.use('/', index);
app.use('/users', users);
app.use('/offres', offres);
    
app.use('/updOffre', updOffre);
app.use('/figurants', figurants);
app.use('/candidatures', candidatures);

app.use('/rest', restapi);



// Configuration de Passport
var Account = require('./models/account');
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
    
module.exports = app;
